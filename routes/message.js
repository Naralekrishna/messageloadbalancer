var express = require('express');
var router = express.Router();

const messageService = require(`./../services/messageService`);

router.post('/send', async (req, res, next) => {
  try {
    const messageSent = await messageService.sendMessage(req.body);
    res.status(200).json({ message: `Messages sent successfully` });
  } catch (error) {
    res.status(400).json({ message: error.message, error });
  }
});

module.exports = router;
