// Imports
const MessagingLoadBalancer = require(`./messagingLoadBalancer`);

const sendMessage = async ({ messages_target, jio_message_limit, vi_message_limit, airtel_message_limit }) => {
    try {
        if (messages_target === '' || messages_target === null || messages_target === undefined || messages_target === 0 || isNaN(messages_target))
            throw new Error(`message target should not be 0, null, undefined, string and blank string`);
        if (jio_message_limit === '' || jio_message_limit === null || jio_message_limit === undefined || isNaN(jio_message_limit))
            throw new Error(`jio message limit should not be 0, null, string and undefined`);
        if (vi_message_limit === '' || vi_message_limit === null || vi_message_limit === undefined || isNaN(vi_message_limit))
            throw new Error(`vi message limit should not be 0, null, string and undefined`);
        if (airtel_message_limit === '' || airtel_message_limit === null || airtel_message_limit === undefined || isNaN(airtel_message_limit))
            throw new Error(`airtel message limit should not be 0, null, string and undefined`);

        const msgLoadBalancer = new MessagingLoadBalancer(messages_target, jio_message_limit, vi_message_limit, airtel_message_limit);
        msgLoadBalancer.sendMessage();
    } catch (error) {
        throw new Error(error.message);
    }
}

module.exports = {
    sendMessage
}