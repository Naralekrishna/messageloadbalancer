module.exports = function MessageLoadBalancer(target, providerOne, providerTwo, providerThree) {
    try {
        this.target = target;
        this.providerOne = providerOne;
        this.providerTwo = providerTwo;
        this.providerThree = providerThree;

        this.sendMessage = async () => {
            console.log(`Message sent`, target, providerOne);
            return true;
        }

        this.sendMessageByJio = async () => {
            console.log(`Message sent by Jio`);
        }

        this.sendMessageByVI = async () => {
            console.log(`Message sent by VI`);
        }

        this.sendMessageByAirtel = async () => {
            console.log(`Message sent by Airtel`);
        }
    } catch (error) {
        throw new Error(error.message);
    }
}