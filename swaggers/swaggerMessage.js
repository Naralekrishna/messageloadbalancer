/**
 * @swagger
 * definitions:
 *   send:
 *     properties:
 *       messages_target:
 *          type: number
 *          example: 300
 *       jio_message_limit:
 *         type: number
 *         example: 100
 *       vi_message_limit:
 *         type: number
 *         example: 100
 *       airtel_message_limit:
 *         type: number
 *         example: 100
 *
 */

/**
 * @swagger
 * /message/send:
 *   post:
 *     tags:
 *       - Message
 *     description: Send message
 *     consumes:
 *       - application/json
 *     requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  $ref: '#/definitions/send'
 *     responses:
 *       200:
 *         description: Message sent
 *       400:
 *         description: Sending message failed
 */